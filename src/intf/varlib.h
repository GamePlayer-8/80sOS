#pragma once

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

char* to_pointer(char input);
char to_array(char* input);
char int_to_char(int number);
int size_of_int(int a);
int size_of_char(char* string);
char* connectstr(char* string1, char* string2);