#pragma once

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

void lock_logging_screen();
void unlock_logging_screen();
void print_console(int type, char* string);