#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

int m_adder(int a, int b);
int m_subtract(int a, int b);
int m_divide(int a, int b);
int m_quick_divide(int a, int b);
int m_multiply(int a, int b);
int m_squareroot(int a);
int m_q_squareroot(int a);

int m_ceil(double a);
int m_floor(double a);
int m_round(double a);

int m_abs(int a);
double d_abs(double a);