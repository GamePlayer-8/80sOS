#include "print.h"
#include "logger.h"
#include "math.h"
#include "varlib.h"

void kernel_main() {
    print_clear();
    print_set_color(PRINT_COLOR_BLACK, PRINT_COLOR_CYAN);
    print_str("Welcome to 80sOS!\n");
    unlock_logging_screen();
    print_console(0, "Initializing...");
    char output = int_to_char(6);
    print_console(0, &output);
}
