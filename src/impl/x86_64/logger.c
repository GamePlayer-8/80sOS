#include "print.h"
#include "logger.h"

bool llock = false;

void lock_logging_screen() {
    llock = true;
}

void unlock_logging_screen() {
    llock = false;
}

void print_console(int type, char* string) {
    print_str("\n");
    print_set_color(PRINT_COLOR_WHITE, PRINT_COLOR_BLACK);
    print_str("[");
    if(type == 0) {
        print_set_color(PRINT_COLOR_GREEN, PRINT_COLOR_BLACK);
        print_str("Ok");
    } else if (type == 1)
    {
        print_set_color(PRINT_COLOR_RED, PRINT_COLOR_BLACK);
        print_str("Err");
    } else if (type == 2)
    {
        print_set_color(PRINT_COLOR_YELLOW, PRINT_COLOR_BLACK);
        print_str("Warn");
    } else {
        print_set_color(PRINT_COLOR_BLUE, PRINT_COLOR_BLACK);
        print_str(type);
    }
    print_set_color(PRINT_COLOR_WHITE, PRINT_COLOR_BLACK);
    print_str("] ");
    print_set_color(PRINT_COLOR_GREEN, PRINT_COLOR_BLACK);
    print_str(string);
}