#include "varlib.h"
#include "math.h"

char int_to_char(int value) {
    char output[2] = "XX";
    output[1] = value+'0';
    return output;
}

char* to_pointer(char input) {
    char* output = &input;
    return output;
}

char to_array(char* input) {
    char output = *input;
    return output;
}

int size_of_int(int a) {
    int sizeof_int = (char*)((&a)+1) - (char*)(&a);
    return sizeof_int;
}

int size_of_char(char* string) {
    int sizeof_char = (char*)((&string)+1) - (char*)(&string);
    return sizeof_char;
}

char* connectstr(char* string1, char* string2) {
    char* stringout;
    stringout = string1;
    int char1_size = size_of_char(string1);
    int char2_size = size_of_char(string2);
    int _tick = 0;
    while (_tick != char2_size)
    {
        stringout[char1_size+_tick] = string2[_tick];
        _tick = _tick + 1;
    }
    return stringout;
}