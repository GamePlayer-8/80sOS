#include "math.h"

double PI = 3.14159265;

int m_adder(int a, int b) {
    return a+b;
}

int m_subtract(int a, int b) {
    return a-b;
}

int m_divide(int a, int b) {
    if(b == 0) {
        return 0;
    }
    if(a == 0) {
        return 0;
    }
    return a/b;
}

int m_quick_divide(int a, int b) {
    return a/b;
}

int m_multiply(int a, int b) {
    return a*b;
}

int m_squareroot(int a) {
    int dynamic = 0;
    int mid;

    while (dynamic <= a) /* dump solution, will be fixed soon */
    {
        mid = (dynamic+a)/2;
        if(mid*mid==a) {
            return mid;
        } else if (mid*mid < a) {
            dynamic = dynamic + 1;
        } else {
            return mid;
        }
    }
    return dynamic;
}

int m_q_squareroot(int a) { /* Inverse Square root from Quake III, watch how it works on https://www.youtube.com/watch?v=p8u_k2LIZyo */
    long i;
    int x2, y;
    const int t = 1.5F;

    x2 = a * 0.5F;
    y = a;
    i = *(long * ) &y;
    i = 0x5f3759df - (i >> 1);
    y = * (int * ) &i;
    y = y * ( t - (x2*y*y));
    return y;
}

int m_ceil(double a) {
    int b = a;
    if(a > b) {
        return b+1;
    }
    return b;
}

int m_floor(double a) {
    int b = a;
    if(b > a) {
        return b-1;
    }
    return b;
}

int m_round(double a) {
    int b = a;
    return b;
}

int m_abs(int a) {
    if(a < 0) {
        return a*-1;
    } else {
        return a;
    }
}

double d_abs(double a) {
    if(a < 0) {
        return a*-1;
    } else {
        return a;
    }
}

double d_degreeradian(double input) {
    return PI * input / 180.0;
}

double d_pow(double a, double b) {
    double c = 1.0;
    double i = 0.0;
    for(i = 0; i < b; i++) {
        c = c*a;
    }
    return c;
}

double d_sinus(double input) {
    double dsin, a, b, c;
    dsin = 0.0;
    a = 1.0;
    c = PI/3;
    for(int n = 0; n < 5; n++) {
        b = 2 * n + 1;
        for(int d = 1; d <= b; d++) {
            a *= d;
        }
        dsin += (d_pow(-1, n) / a) * d_pow(c, b);
    }
    return dsin;
}

double d_cosinus(double input) {
    
}