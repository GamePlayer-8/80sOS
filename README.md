# 80sOS
## In short
it's an operating system, on assembler and C language. In future on Cpp and GDScript language.

## OS Status
 WIP - Work in progress
| Packages | State | Actuall progress |
|------------|-------|------------------|
| [Open Package Manager](https://github.com/80sOS/opm) | WIP | Waiting on progress |
| [80sOS graphics](https://github.com/80sOS/80sOS_graphics) | WIP | More graphics coming soon |
| [80sOS](https://github.com/80sOS/80sOS) | WIP | Making libraries for C language |
| [LFS setupscripts](https://github.com/80sOS/LFS-setupscripts) | Abandoned | - |

## Using tutorials
 - [CodePulse](https://www.youtube.com/watch?v=FkrpUaGThTQ&ab_channel=CodePulse)

# Compile Dependencies
## Needed apps/envoirments
The programs what you will need to compile source code:
 - [Docker](https://www.docker.com);
 - Optionally [Git](https://git-scm.com).

## Compile
### On Linux
| Step | Place to execute | Command |
|-|-|-|
| 0. | /bin/bash | cd <place/where/you/want/to/download/sourcecode> |
| 1. | /bin/bash | git clone https://github.com/80sOS/80sOS |
| 2. | /bin/bash | cd "80sOS" |
| 3. | /bin/bash | docker build buildenv -t "eightysos_buildenv" |
| 4. | /bin/bash | docker run --rm -it -v $(pwd):/root/env eightysos_buildenv |
| 5. | in docker container | make build-x86_64 |
| 6. | in docker container | exit |

## Test/run the OS
the ISO File should be in dist/x86_64 folder, called "kernel.iso". You can execute it by [VirtualBox](https://www.virtualbox.org), [QEMU](https://www.qemu.org) or even on your host machine.

### Hardware requirements
#### CPU
 - 1 Ghz or more/less;
 - Support on Long mode;
 - Support on 64-bit architecture;
 - Support on CPU ID;
 - Support on Multiboot.
#### GPU
 - 5 MB of vRam (Virtual Random Access Memory) should be ok.
#### Ram
 - 5 MB of Ram (Random Access Memory) is enough.
#### Disk space
 - 16 MB.

# Future plans
 - Make kernel modules to support devices (cdrom, gpu, usb devices etc);
 - Make filesystem (ext4 i think);
 - Make Instancer (module/app what allows to run more than one app at the same time);
 - Make full support on C language and introduce Cpp, GDScript language;
 - Install bash.
# Note
I'm making this OS, because it's my hobby.
